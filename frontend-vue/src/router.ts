import Vue from 'vue';
import Router from 'vue-router';

import store from './store';
import HomeView from './views/HomeView.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'router-link-active is-active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/signin',
      name: 'signin',
      component: () => import(/* webpackChunkName: "signinView" */ './views/SigninView.vue'),
    },
    {
      path: '/dashboard',
      component: () => import(/* webpackChunkName: "dashboardView" */ './views/DashboardView.vue'),
      children: [
        { path: '', redirect: '/dashboard/deck' },
        {
          path: 'deck',
          name: 'deckList',
          component: () => import(/* webpackChunkName: "deckListView" */ './views/DeckListView.vue'),
          children: [
            {
              path: '',
              component: () => import(/* webpackChunkName: "wrDeckList" */ './components/WrDeckList.vue'),
            },
            {
              path: ':id',
              component: () => import(/* webpackChunkName: "wrDeckDetail" */ './components/WrDeckDetail.vue'),
            },
          ],
        },
        {
          path: 'room',
          name: 'roomList',
          component: () => import(/* webpackChunkName: "roomListView" */ './views/RoomListView.vue'),
          children: [
            {
              path: ':id',
              name: 'roomDetail',
              component: () => import(/* webpackChunkName: "wrRoomDetail" */ './components/WrRoomDetail.vue'),
            },
          ],
        },
      ],
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.path.includes('dashboard') && !store.state.currentUser) {
    next('/');
  }
  next();
});

export default router;
