import Vue from 'vue';
import VeeValidate from 'vee-validate';

import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import { createProvider } from './vue-apollo';

Vue.config.productionTip = false;

Vue.use(VeeValidate);

export default new Vue({
  router,
  store,
  apolloProvider: createProvider({
    httpLinkOptions: {
      credentials: 'same-origin',
    },
    // getAuth,
  }),
  render: (h) => h(App),
}).$mount('#app');
