declare var gapiDeferred: Promise<any>;
declare var grecaptchaDeferred: Promise<any>;
declare var FBDeferred: Promise<any>;
declare var localStorage: Storage;
declare var window: Window;

declare module 'http';
declare module 'ws';
declare module 'node';
declare module 'vue-cli-plugin-apollo/graphql-client';
