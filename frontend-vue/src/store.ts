import Vue from 'vue';
import Vuex from 'vuex';

import { onLogin, onLogout } from './vue-apollo';
import vue from './main';

Vue.use(Vuex);

export default new Vuex.Store({
  state: ({
    token: null,
    currentUser: null,
    userDecks: [],
  } as {
    token: string | null,
    currentUser: any,
    userDecks: any[],
  }),
  mutations: {
    stateSignin(state, { token, user, vm }) {
      state.currentUser = user;
      state.token = token;
    },
    stateSignout(state) {
      state.currentUser = null;
      state.token = null;
      state.userDecks = [];
    },
  },
  actions: {
    async signin({ commit }, { token, user }) {
      // https://github.com/Akryum/vue-apollo/issues/409
      await onLogin((vue.$apollo.provider as any).defaultClient, token);
      commit('stateSignin', { token, user });
    },
    async signout({ commit }) {
      await onLogout((vue.$apollo.provider as any).defaultClient);
      commit('stateSignout');
    },
  },
});
