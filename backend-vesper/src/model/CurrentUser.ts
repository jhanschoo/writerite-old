import { Column, Index, PrimaryGeneratedColumn } from 'typeorm';
import { IsEmail } from 'class-validator';

export class CurrentUser {

  @PrimaryGeneratedColumn('uuid')
  public id?: string;

  @IsEmail()
  @Column({ unique: true })
  @Index({ unique: true })
  public email?: string;

  @Column({ type: 'simple-array' })
  public roles?: string[];

  public constructor(o?: CurrentUser) {
    this.id = o && o.id;
    this.email = o && o.email;
    this.roles = o && o.roles;
  }
}
