import { Authorized, Controller, Query, Mutation } from 'vesper';
import { EntityManager } from 'typeorm';
import { Deck } from '../entity/Deck';
import { CurrentUser } from '../model/CurrentUser';

@Controller()
export class DeckController {
  constructor(
    private entityManager: EntityManager,
    private currentUser: CurrentUser,
  ) {}

  // userDecks: [Deck!]
  @Authorized()
  @Query()
  public async userDecks() {
    return this.entityManager.find(Deck, {
      relations: ['owner'],
      where: {
        owner: this.currentUser.id,
      },
    });
  }

  // deck(id: String): Deck
  @Query()
  public deck({ id }: { id: string }) {
    return this.entityManager.findOne(Deck, id);
  }

  // deckSave(id: ID, name: String!): Deck
  @Authorized()
  @Mutation()
  public async deckSave({ id, name }: { id?: string, name: string }) {
    if (id) {
      const deck = await this.entityManager.findOne(Deck, id);
      if (deck && deck.owner === this.currentUser.id) {
        deck.name = name;
        return this.entityManager.save(Deck, deck);
      }
      return null;
    } else {
      const deck = this.entityManager.create(Deck, { name, owner: { id: this.currentUser.id } });
      return this.entityManager.save(Deck, deck);
    }
  }

  // deckDelete(id: ID!): Boolean!
  @Authorized()
  @Mutation()
  public async deckDelete(id: { id: string }) {
    const deck = await this.entityManager.findOne(Deck, id);
    if (deck && deck.owner === this.currentUser.id) {
      await this.entityManager.remove(Deck, deck);
      return true;
    }
    return false;
  }
}
