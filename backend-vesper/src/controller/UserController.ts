import { EntityManager } from 'typeorm';
import { Controller, Query, Authorized } from 'vesper';
import { CurrentUser } from '../model/CurrentUser';
import { User } from '../entity/User';

@Controller()
export class UserController {
  constructor(
    private entityManager: EntityManager,
    private currentUser: CurrentUser) {
  }

  // users: [User!]!
  @Query()
  @Authorized(['admin'])
  public users() {
    return this.entityManager.find(User);
  }

  // user(id: String!): User
  @Query()
  public user({ id }: { id: string }) {
    return this.entityManager.findOne(User, id);
  }
}
