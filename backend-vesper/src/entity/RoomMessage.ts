import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { User } from './User';
import { Deck } from './Deck';
import { Room } from './Room';

@Entity()
export class RoomMessage {
  @PrimaryGeneratedColumn('uuid')
  public id?: string;

  @Column()
  public content?: string;

  @ManyToOne((type) => Room, (room) => room.messages)
  public room?: Room;

  public get readMessage(): any {
    return {
      content: this.content,
    };
  }

  // TODO: timestamp, etc.
}
